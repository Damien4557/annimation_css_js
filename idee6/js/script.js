var arrow = document.getElementsByClassName("arrow");
var input = document.getElementsByClassName("input");
var body = document.getElementById("body");

for (let i = 0; i <= input.length; i++) {
  const arrowElement = arrow[i];
  const inputElement = input[i];
  let isClick = false;

  inputElement.addEventListener("mousedown", (e) => {
    
    isClick = true;
  });
  window.addEventListener("mousemove", (e) => {
    let x = 0;
    let y = 0;
    if (isClick === true) {
      x = e.offsetX;
      y = e.offsetY;
      x = x - 50;
      y = y - 20;
      console.log(x, y);
      moduls.style.left = x + "px";
      moduls.style.top = y + "px";
      moduls.style.cursor = "grab";
    }
  });
  inputElement.addEventListener("mouseup", (e) => {
    if (isClick === true) {
      isClick = false;
      moduls.style.cursor = "pointer";
    }
  });

  arrowElement.animate(
    [
      {
        transform: "translateX(0)",
      },
      {
        transform: "translateX(10px)",
      },
      {
        transform: "translateX(0px)",
      },
    ],
    {
      duration: 1000,
      iterations: Infinity,
    }
  );
}
